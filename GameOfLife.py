#!/usr/bin/python

import random
import time
from subprocess import call
import platform


class Grid:
    def __init__(self, emp, occ):
        self.grid = []
        self.emp = emp
        self.occ = occ
        self.stats = [1, 1, 1, 1, 1, 1]

    def make(self, xc, yc):
        y = []
        while len(y) != yc:
            x = []
            while len(x) != xc:
                x.append(self.emp)
            y.append(x)
        self.grid = y

    def print(self):
        live_amount=0
        dead_amount=0
        for y in self.grid:
            for x in y:
                if x == self.occ:
                    live_amount+=1
                elif x == self.emp:
                    dead_amount+=1
                print(x, end="", flush=True)
                #time.sleep(0.000001)
            print("")
        print("-" * len(self.grid[0]))
        print(f"Live cells: {live_amount} | Dead cells: {dead_amount} | Births: {self.stats[0]} | Deaths: {self.stats[1]} | Birth/death ratio: {round(self.stats[0]/self.stats[1], 2)} | Total births: {self.stats[2]} | Total deaths: {self.stats[3]} | Generation: {self.stats[4]} | Generations/sec: {self.stats[5]}")

    def maketest(self, xc, yc):
        i = 0
        y = []
        while len(y) != yc:
            x = []
            while len(x) != xc:
                x.append(f"{i} ")
                i += 1
            y.append(x)
        self.grid = y

    def rfill(self):
        yc = 0
        for y in self.grid:
            xc = 0
            for x in y:
                if random.getrandbits(1) == 1:
                    self.grid[yc][xc] = self.occ
                xc += 1
            yc += 1

    def neighbours(self, x, y, incl):
        neighbours = []
        lenx = len(self.grid[y]) - 1
        leny = len(self.grid) - 1

        if (x > 0) and (y > 0):
            neighbours.append(self.grid[y - 1][x - 1])

        if y > 0:
            neighbours.append(self.grid[y - 1][x])

        if (y > 0) and (x < lenx):
            neighbours.append(self.grid[y - 1][x + 1])

        if x > 0:
            neighbours.append(self.grid[y][x - 1])

        if incl == True:
            neighbours.append(self.grid[y][x])

        if x < lenx:
            neighbours.append(self.grid[y][x + 1])

        if (y < leny) and (x > 0):
            neighbours.append(self.grid[y + 1][x - 1])

        if y < leny:
            neighbours.append(self.grid[y + 1][x])

        if (y < leny) and (x < lenx):
            neighbours.append(self.grid[y + 1][x + 1])

        return neighbours


def gollogic(grid):
    yc = 0
    rgrid = Grid(" ", "0")
    births = 0
    deaths = 0
    rgrid.make(len(grid.grid[0]), len(grid.grid))
    for y in grid.grid:
        xc = 0
        for x in y:
            n = grid.neighbours(xc, yc, False)
            xcount = n.count(grid.occ)

            if (xcount < 2) and x == grid.occ:
                rgrid.grid[yc][xc] = grid.emp
                deaths += 1

            elif ((xcount == 2) or (xcount == 3)) and x == grid.occ:
                rgrid.grid[yc][xc] = grid.occ

            elif (xcount > 3) and x == grid.occ:
                rgrid.grid[yc][xc] = grid.emp
                deaths += 1

            elif (x == grid.emp) and (xcount == 3):
                rgrid.grid[yc][xc] = grid.occ
                births += 1

            xc += 1
        yc += 1
    global total_births
    global total_deaths
    global generation
    global timer
    generation += 1
    total_births += births
    total_deaths += deaths
    gen_per_sec=round((1/(timer[1]-timer[0])), 1)


    grid.stats = [births, deaths, total_births, total_deaths, generation, gen_per_sec]
    return rgrid.grid

generation = 0
grid = Grid(" ", "0")
total_births = 0
total_deaths = 0
timer = [0, 0]
grid.make(212, 52)
grid.rfill()
grid.print()

if platform.system() == "Windows":
    clear = "cls"
else:
    clear = "clear"

while True:
    #time.sleep(0.1)
    del timer[0]
    timer.append(time.time())
    grid.grid = gollogic(grid)
    call(clear)
    grid.print()
