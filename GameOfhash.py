#!/usr/bin/python

import random
from subprocess import call
import base64


state = random.getstate()


class Grid:
    def __init__(self, emp, occ):
        self.grid = []
        self.emp = emp
        self.occ = occ

    def make(self, xc, yc):
        y = []
        while len(y) != yc:
            x = []
            while len(x) != xc:
                x.append(self.emp)
            y.append(x)
        self.grid = y

    def print(self):
        for y in self.grid:
            for x in y:
                print(x, end="", flush=True)
            print("")
        print("-" * len(self.grid[0]))

    def maketest(self, xc, yc):
        i = 0
        y = []
        while len(y) != yc:
            x = []
            while len(x) != xc:
                x.append(f"{i} ")
                i += 1
            y.append(x)
        self.grid = y

    def rfill(self):
        yc = 0
        for y in self.grid:
            xc = 0
            for x in y:
                if random.getrandbits(1) == 1:
                    self.grid[yc][xc] = self.occ
                xc += 1
            yc += 1

    def neighbours(self, x, y, incl):
        neighbours = []
        lenx = len(self.grid[y]) - 1
        leny = len(self.grid) - 1

        if (x > 0) and (y > 0):
            neighbours.append(self.grid[y - 1][x - 1])

        if y > 0:
            neighbours.append(self.grid[y - 1][x])

        if (y > 0) and (x < lenx):
            neighbours.append(self.grid[y - 1][x + 1])

        if x > 0:
            neighbours.append(self.grid[y][x - 1])

        if incl == True:
            neighbours.append(self.grid[y][x])

        if x < lenx:
            neighbours.append(self.grid[y][x + 1])

        if (y < leny) and (x > 0):
            neighbours.append(self.grid[y + 1][x - 1])

        if y < leny:
            neighbours.append(self.grid[y + 1][x])

        if (y < leny) and (x < lenx):
            neighbours.append(self.grid[y + 1][x + 1])

        return neighbours


def gollogic(grid):
    yc = 0
    rgrid = Grid(" ", "0")
    rgrid.make(len(grid.grid[0]), len(grid.grid))
    for y in grid.grid:
        xc = 0
        for x in y:
            n = grid.neighbours(xc, yc, False)
            xcount = n.count(grid.occ)

            if (xcount < 2) and x == grid.occ:
                rgrid.grid[yc][xc] = grid.emp
            elif ((xcount == 2) or (xcount == 3)) and x == grid.occ:
                rgrid.grid[yc][xc] = grid.occ
            elif (xcount > 3) and x == grid.occ:
                rgrid.grid[yc][xc] = grid.emp

            elif (x == grid.emp) and (xcount == 3):
                rgrid.grid[yc][xc] = grid.occ
            xc += 1
        yc += 1
    return rgrid.grid


def encode(key, clear):
    enc = []
    for i in range(len(clear)):
        key_c = key[i % len(key)]
        enc_c = chr((ord(clear[i]) + ord(key_c)) % 256)
        enc.append(enc_c)
    return base64.urlsafe_b64encode("".join(enc).encode()).decode()


def decode(key, enc):
    dec = []
    enc = base64.urlsafe_b64decode(enc).decode()
    for i in range(len(enc)):
        key_c = key[i % len(key)]
        dec_c = chr((256 + ord(enc[i]) - ord(key_c)) % 256)
        dec.append(dec_c)
    return "".join(dec)

eord=input("[e]ncrypt or [d]ecrypt?")
msg=input("Message: ")
seed=input("Password: ")
random.seed(seed)
grid = Grid(" ", "0")
grid.make(256, 256)
grid.rfill()

hashvar = ""

for n in range(16):
    grid.grid = gollogic(grid)
    for y in grid.grid:
        i=0
        for x in y:
            i += x.count(grid.occ)
        hashvar += str(i)

if eord == "e":
    msg = encode(hashvar, msg)

elif eord == "d":
    msg = decode(hashvar, msg)

print(msg)

